import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please enter the square side size: ");
		int squareSide = scanner.nextInt();
		System.out.print("Please enter the rectangle length: ");
		int rectLength = scanner.nextInt();
		System.out.print("Please enter the rectangle width: ");
		int rectWidth = scanner.nextInt();
		AreaComputations areaComp = new AreaComputations();
		System.out.println("The area of the square is " + AreaComputations.areaSquare(squareSide));
		System.out.println("The area of the rectangle is " + areaComp.areaRectangle(rectLength, rectWidth));
	}
}